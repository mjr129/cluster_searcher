

def main():
    # noinspection PyUnresolvedReferences
    import cluster_searcher
    from intermake import start
    start()
    
if __name__=="__main__":
    main()