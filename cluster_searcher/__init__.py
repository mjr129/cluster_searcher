__version__ = "0.0.0.42"

import intermake


CLUSTER_SEARCHER_APP = intermake.Environment( name = "cluster_searcher",
                                              version = __version__ )

from cluster_searcher import commands
